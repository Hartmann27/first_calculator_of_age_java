import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);

        System.out.println("What year is now?");
        var yearBth = scanner.nextDouble();

        while (true) {
            System.out.println("What is year of your birth? ");
            var yearNow = scanner.nextDouble();

            if (yearNow == 0)
                break;

            var result = yearBth - yearNow;
            System.out.println("Your age is " + result + "years old!");
        }
    }
}